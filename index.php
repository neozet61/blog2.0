<?php

// Include libraries
include_once "./include/function/help_functions.php";
include_once "./include/function/storage_functions.php";

// Entity Constants
const FIELD_NUM_POST_ID = 0;
const FIELD_NUM_POST_TITLE = 1;
const FIELD_NUM_POST_BODY = 2;
const FIELD_NUM_POST_CATEGORY_ID = 3;
const FIELD_NUM_POST_TIME = 4;

const FIELD_NUM_CATEGORY_ID = 0;
const FIELD_NUM_CATEGORY_TITLE = 1;

const FIELD_NUM_COMMENT_ID = 0;
const FIELD_NUM_COMMENT_USER_NICKNAME = 1;
const FIELD_NUM_COMMENT_BODY = 2;
const FIELD_NUM_COMMENT_TIME = 3;
const FIELD_NUM_COMMENT_POST_ID = 4;

const FIELD_NUM_USER_ID = 0;
const FIELD_NUM_USER_NICKNAME = 1;
const FIELD_NUM_USER_LOGIN = 2;
const FIELD_NUM_USER_PASSWORD = 3;
const FIELD_NUM_USER_TIME = 4;

//Проверка кук
$users = getAll('users');

//Получение категорий для сайдбара
$categories = getAll('categories');
$categoryTitleById = [];
foreach ($categories as $category) {
    $categoryTitleById[$category[FIELD_NUM_CATEGORY_ID]] = $category[FIELD_NUM_CATEGORY_TITLE];

}


if(isset($_COOKIE["login"]) and isset($_COOKIE['password'])){
    foreach ($users as $user) {
        $userId = $user[FIELD_NUM_USER_ID];
        $userNickname = $user[FIELD_NUM_USER_NICKNAME];
        $userLogin = $user[FIELD_NUM_USER_LOGIN];
        $userPassword = $user[FIELD_NUM_USER_PASSWORD];
        $userTime = $user[FIELD_NUM_USER_TIME];

        if ($userLogin == $_COOKIE["login"] and $userPassword == $_COOKIE["password"]) {
            $isAuthed = TRUE;
            break;
        } else {
            $isAuthed = FALSE;
            break;
        }
    }
}else{
    $isAuthed = FALSE;
}


// Выбор контроллера
$page = null;

if (isset($_GET['page'])) {
    $page = $_GET['page'];
}

switch ($page) {
    case null:
        include "./include/controllers/main-page-controller.php";
        break;

    //просмотр категории
    case "category-view":
        include "./include/controllers/category-result-controller.php";
        break;

    //форма регистрации
    case "registration":
        include "./include/controllers/registration-controller.php";
        break;

    //создание комментария
    case "user-save":
        include "./include/controllers/user-save-controller.php";
        break;

        //форма логина
    case "auth-form":
        include "./include/controllers/auth-form-controller.php";
        break;

    //auth-check - проверка логина и пароля
    case "auth-check":
        include "./include/controllers/auth-check-controller.php";
        break;

    //выход из авторизации
    case "exit-controller":
        include "./include/controllers/exit-controller.php";
        break;

    // Страница просмотра поста
    case "post-view":
        include "./include/controllers/post-view-controller.php";
        break;

    // Страница формы для создания поста
    case "post-create":
        include "./include/controllers/post-create-controller.php";
        break;

    // Сабмитим сюда форму сохранения поста
    case "post-save":
        include "./include/controllers/post-save-controller.php";
        break;

        //сохранение комментария
    case "comment-save":
        include "./include/controllers/comment-save-controller.php";
        break;

    //настройки
    case "settings-show":
        include "./include/controllers/settings-show-controller.php";
        break;

    case "settings-save":
        include "./include/controllers/settings-save-controller.php";
        break;

    default:
        die("Такой страницы нет. Ошибка.");
}

/* main-page - главная страница

auth-form - форма логина
auth-check - проверка логина и пароля

category-view - просмотр категории

post-view - просмотр поста
post-create - создание поста
post-save - сохранение поста

comment-save - создание комментария

*/
<?php

$postId = $_POST['post_id'];
$postTitle = trim(strip_tags($_POST['post_title']));
$postBody = trim(strip_tags($_POST['post_body']));


if (empty($postTitle)) {header("Location: index.php?page=post-create&post_title=warning");}

if (empty($postBody)) {header("Location: index.php?page=post-create&post_body=warning");}

if ($_POST['category'] == null) {header("Location: index.php?page=post-create&id_category=warning");}

$counter = getMaxIdForSpace('posts') + 1;
insert('posts', $counter, [$counter, $postTitle, $postBody, $_POST['category'], time()]);
header("Location: index.php?page=post-view&id_post=$counter");





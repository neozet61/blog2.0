<?php

$categories = getAll('categories');

foreach ($categories as $category) {
    $categoryTitleById[$category[FIELD_NUM_CATEGORY_ID]] = $category[FIELD_NUM_CATEGORY_TITLE];
}

include "./include/views/settings-show-view.php";
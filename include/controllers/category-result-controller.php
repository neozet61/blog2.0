<?php


$categoryTitleById = [];

$categories = getAll('categories');

$posts = selectWhereFieldEqual('posts', FIELD_NUM_POST_CATEGORY_ID, $_GET['id_category']);

arsort($posts);

foreach ($categories as $category) {
    $categoryTitleById[$category[FIELD_NUM_CATEGORY_ID]] = $category[FIELD_NUM_CATEGORY_TITLE];

}

include "./include/views/category-result-view.php";

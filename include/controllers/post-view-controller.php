<?php

$post = getById('posts', $_GET['id_post']);

if (is_null($post)) {
    die('Sorry, пост не найден');
}

$postId = $post[FIELD_NUM_POST_ID];
$postTitle = $post[FIELD_NUM_POST_TITLE];
$postBody = $post[FIELD_NUM_POST_BODY];
$postCategoryId = $post[FIELD_NUM_POST_CATEGORY_ID];
$postTime = $post[FIELD_NUM_POST_TIME];

setcookie("lastvisit_" . $postId, time(), time() + 86400);

$categories = getAll('categories');

foreach ($categories as $category) {
    $categoryTitleById[$category[FIELD_NUM_CATEGORY_ID]] = $category[FIELD_NUM_CATEGORY_TITLE];
}

$comments = selectWhereFieldEqual('comments', FIELD_NUM_COMMENT_POST_ID, $postId);

asort($comments);

$commentClass = "comment";

include "./include/views/post-view.php";


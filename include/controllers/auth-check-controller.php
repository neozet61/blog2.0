<?php

$inputLogin = $_POST['login'];
$inputPassword = $_POST['password'];

$storageUsers = selectWhereFieldEqual('users', FIELD_NUM_USER_LOGIN, $inputLogin);

if (count($storageUsers) == 0) {
    header("Location: index.php?page=auth-form&warning=user+is+not+exists");
    die();
}

$storageUser = $storageUsers[0];

if ($storageUser[FIELD_NUM_USER_PASSWORD] !== $inputPassword) {
    header("Location: index.php?page=auth-form&warning=bad+password");
    die();
}

$cookieExpiresAt = time() + 60 * 60 * 24;

setcookie("login", $storageUser[FIELD_NUM_USER_LOGIN], $cookieExpiresAt);
setcookie('password', $storageUser[FIELD_NUM_USER_PASSWORD], $cookieExpiresAt);

header("Location: index.php");
<?php

$nickname = strip_tags($_POST['nickname']);
$login = strip_tags($_POST['login']);
$password = $_POST['password'];
$repeat_password = $_POST['repeat_password'];

if ($password !== $repeat_password){
    header("Location: registration.php?password=warning");
}

$counter = getMaxIdForSpace('users') + 1;
insert('users', $counter, [$counter, $nickname, $login, $password, time()]);
setcookie("login", $login, time() + 86400);
setcookie('password', $password, time() + 86400);
header("Location: index.php");
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>MyBlog</title>
    <link rel="stylesheet" href="styles.css">
</head>
<body>

<table width="90%" border="0" align="center" bgcolor="white" cellspacing="10">
    <tr>
        <td colspan="5" align="left" width="80%">
            <h1><font color="gray">Главная</font></h1>
            <hr>
        </td>
        <td colspan="1">
            <?php if (!$isAuthed): ?>
                <a class='bot2' href='index.php?page=auth-form'>Войти</a>
            <?php else: ?>

                <h4><font color='gray'>Hello, <?= $user[FIELD_NUM_USER_NICKNAME] ?>!</font></h4>
                <a class='bot2' href='index.php?page=exit-controller'>Выйти</a>
            <?php endif; ?>
        </td>
    </tr>
    <tr>
        <td colspan="5" width="80%">
            <ul>
                <?php
                foreach ($posts as $post): ?>
                    <h2><?= $post[FIELD_NUM_POST_TITLE] ?></h2>
                        <p>
                            <b>Категория: </b> <?= $categoryTitleById[$post[FIELD_NUM_POST_CATEGORY_ID]] ?> |
                            <b>Дата:</b> <?= date('d.m.Y H:i', $post[FIELD_NUM_POST_TIME]) ?>
                        </p>

                        <p><?= $post[FIELD_NUM_POST_BODY] ?></p>

                        <a href="index.php?page=post-view&id_post=<?= $post[FIELD_NUM_POST_ID] ?>">Перейти к посту:
                            <?= $post[FIELD_NUM_POST_TITLE] ?>
                        </a>
                        <br><br>
                <?php endforeach; ?>
            </ul>
        </td>
        <td colspan="1" valign="top" align="left">
            <h2>Категории:</h2>
            <ul>
                <?php include "./include/views/parts/nav-part-category.php"; ?>
            </ul>
            <?php if ($isAuthed): ?>
                <h2>Функции:</h2>
                <ul>
                    <li><a href=index.php?page=post-create>Добавить пост</a></li>
                </ul>
            <?php endif; ?>
            <h2>Дополнительно:</h2>
            <ul>
                <li><a href=index.php?page=settings-show>Выбор фона</a></li>
            </ul>
        </td>
    </tr>
</body>
</html>
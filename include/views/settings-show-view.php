<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Создание поста</title>
    <link rel="stylesheet" href="styles.css">
    <style>
        body {
        <?= $background ?>
        }
    </style>
</head>
<body>
<table width="90%" border="0" align="center" bgcolor="white">
    <tr>
        <td colspan="5" align="left" width="80%">
            <h1><font color="gray">Выбор фона</font></h1>
            <hr>
        </td>
        <td colspan="1" align="left">
            <p>
                <a class="bot2" href="index.php">Главная</a>
            </p>
        </td>
    </tr>
    <tr>
        <td colspan="5" width="80%">
            <form action="index.php?page=settings-save" method="post">
                <div class="form-login">
                    <label for="background">Выбери цвет фона:</label><br>
                    <p><input type="radio" name="background" value="image"
                            <?php if (isset($_COOKIE["background"]) and $_COOKIE["background"] == "image" ){echo "checked";} ?>
                        >Картинка</p>
                    <p><input type="radio" name="background" value="blue"
                            <?php if ($_COOKIE["background"] == "blue" ){echo "checked";} ?>
                        >Синий</p>
                    <p><input type="radio" name="background" value="red"
                            <?php if ($_COOKIE["background"] == "red" ){echo "checked";} ?>
                        >Красный</p>
                    <p><input type="radio" name="background" value="grey"
                            <?php if ($_COOKIE["background"] == "grey" ){echo "checked";} ?>
                        >Серый</p>
                </div>
                <div class="form-login">
                    <button type="submit">Сохранить</button>
                </div>
            </form>
            <br>
        </td>
        <td colspan="1" valign="top" align="left">
            <h2><font color="gray">Категории:</font></h2>
            <ul>
                <?php include "./include/views/parts/nav-part-category.php"; ?>
            </ul>
        </td>
    </tr>
</table>
</body>
</html>

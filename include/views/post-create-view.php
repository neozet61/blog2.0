<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Создание поста</title>
    <link rel="stylesheet" href="styles.css">
</head>
<body>
<table width="90%" border="0" align="center" bgcolor="white">
    <tr>
        <td colspan="5" align="left" width="80%">
            <h1><font color="gray">Создание поста</font></h1>
            <hr>
        </td>
        <td colspan="1" align="left">
            <p>
                <a class="bot2" href="index.php">Главная</a>
            </p>
        </td>
    </tr>
    <tr>
        <td colspan="5" width="80%">
            <?php if ($isAuthed): ?>
                <form action="index.php?page=post-save" method="post">

                    <div class="form-login">
                        <label for="post_title">Название поста:</label><br>
                        <input id="post_title" name="post_title" required placeholder="Заголовок">

                        <?php if (isset($_GET['post_title'])): ?>
                            "<p> Извини, но необходимо ввести заголовок!</p>
                        <?php endif; ?>
                    </div>

                    <div class="form-login">
                        <label for="category">Категория:</label><br>
                        <select size="1" required name="category">
                            <option selected disabled value="none" hidden="">Выберите категорию</option>

                            <?php foreach ($categoryTitleById as $categoryId => $categoryTitle): ?>
                                <option value='<?= $categoryId ?>'><?=$categoryTitle?></option>
                            <?php endforeach; ?>

                        </select>

                        <?php if (isset($_GET['id_category'])): ?>
                            <p> Извини, но необходимо выбрать категорию!</p>
                        <?php endif; ?>
                    </div>
                    <div class="form-login">
                        <label for="post_body">Текст пост:</label><br>
                        <textarea name="post_body" id="post_body" required placeholder="Я вот что думаю..."></textarea>
                        <input type="hidden" name="post_id" value="<?php echo $postId; ?>">

                        <?php if (isset($_GET['post_body'])): ?>
                            <p> Извини, но необходимо ввести содержимое поста!</p>
                        <?php endif; ?>

                    </div>

                    <div class="form-login">
                        <button type="submit">Опубликовать</button>
                    </div>

                </form>
                <br>
            <?php else: ?>
                <h2>Для добавления поста необходима авторизация!</h2>
                <p>Авторизируйся по братски ;)</p>
                <a class="bot2" href="login.php">Войти</a>
            <?php endif; ?>
        </td>
        <td colspan="1" valign="top" align="left">
            <h2><font color="gray">Категории:</font></h2>
            <ul>
                <?php include "./include/views/parts/nav-part-category.php"; ?>
            </ul>
        </td>
    </tr>

</table>
</body>
</html>

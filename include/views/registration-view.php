<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Login</title>
    <link rel="stylesheet" href="styles.css">
</head>
<body>
<table width="50%" border="0" align="center" bgcolor="white" cellspacing="10">
    <tr>
        <td colspan="1" align="left" width="80%">
            <h2><font color="gray">Регистрация</font></h2>
            <hr>
        </td>
        <td colspan="1" align="left">

            <a class="bot2" href="index.php">Главная</a>
            <a class="bot2" href="index.php?page=auth-form">Войти</a>
        </td>
    </tr>
    <tr>
        <td colspan="2" width="80%">
            <form action="index.php?page=user-save" method="post">
                <div class="form-login">
                    <label for="nickname">Придумай никнейм:</label><br>
                    <input id="nickname" name="nickname" required placeholder="nickname">
                </div>

                <div class="form-login">
                    <label for="login">Какой логин хочешь?</label><br>
                    <input id="login" name="login" required placeholder="Login">
                </div>

                <div class="form-login">
                    <label for="password">Ну и пароль не забудь:</label><br>
                    <input type="password" id="password" name="password" required placeholder="Password">
                    <?php
                    if (isset($_GET['password'])) {
                        echo "<p>Очень жаль, но пароли не совпадают!</p>";
                    }
                    ?>
                </div>

                <div class="form-login">
                    <label for="repeat_password">Повтори пароль:</label><br>
                    <input type="password" id="repeat_password" name="repeat_password" required placeholder="Repeat password">
                </div>

                <div class="form-login">
                    <button type="submit">Зарегистрироваться</button>
                </div>

            </form>
            <br>
        </td>

    </tr>

</table>
</body>
</html>

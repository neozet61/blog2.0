<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>MyBlog</title>
    <link rel="stylesheet" href="styles.css">
    <style>
        body {
            font-family: Arial;
        }
    </style>
</head>
<body>

<table width="90%" border="0" align="center" bgcolor="white">
    <tr>
        <td colspan="5" align="left">
            <h1><font color="gray">Категория: <?= $categoryTitleById[$_GET['id_category']] ?></font></h1>
            <hr>
        </td>
        <td colspan="1" align="left">
            <a class="bot2" href="index.php">Главная</a>
        </td>
    </tr>
    <tr>
        <td colspan="5" width="80%">
            <ul>
                <?php foreach ($posts as $post) :
                        $postId = $post[FIELD_NUM_POST_ID];
                        $postTitle = $post[FIELD_NUM_POST_TITLE];
                        $postBody = $post[FIELD_NUM_POST_BODY];
                        $postCategoryId = $post[FIELD_NUM_POST_CATEGORY_ID];
                        $postTime = $post[FIELD_NUM_POST_TIME];
                        ?>

                    <h2><?=$postTitle?></h2>

                    <p>
                        <b>Категория: </b><?=$categoryTitleById[$postCategoryId]?> | <b>Дата:</b> " .
                        <?=date('d.m.Y H:i', $postTime)?>
                    </p>
                    <p><?=$postBody?></p>
                    <a href=index.php?page=post-view&id_post=<?=$postId?>>Перейти к посту: <?=$postTitle?></a>
                    <br><br>
                <?php endforeach; ?>
            </ul>
        </td>
        <td colspan="1" valign="top" align="left">
            <h2>Категории</h2>
            <ul>
                    <?php include "./include/views/parts/nav-part-category.php"; ?>
            </ul>
        </td>
    </tr>
</body>
</html>
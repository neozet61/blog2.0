<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Login</title>
    <link rel="stylesheet" href="styles.css">
    <style>
        body {
        <?= $background ?>
        }
    </style>
</head>
<body>
<table width="50%" border="0" align="center" bgcolor="white" cellspacing="10">
    <tr>
        <td colspan="1" align="left" width="80%">
            <h2><font color="gray">Login</font></h2>
            <hr>
        </td>
        <td colspan="1" align="left">

            <a class="bot2" href="index.php">Главная</a>
            <a class="bot2" href="index.php?page=registration">Регистрация</a>
        </td>
    </tr>
    <tr>
        <td colspan="2" width="80%">
            <form action="index.php?page=auth-check" method="post">

                <div class="form-login">
                    <?php
                    if (isset($_GET['warning'])) {
                        echo "<p>{$_GET['warning']}</p>";
                    }
                    ?>
                    <label for="login">Введи логин братишка:</label><br>
                    <input id="login" name="login" required placeholder="Login">
                </div>

                <div class="form-login">
                    <label for="password">Ну и пароль не забудь:</label><br>
                    <input type="password" id="password" name="password" required placeholder="Password">
                </div>

                <div class="form-login">
                    <button type="submit">Войти</button>
                </div>

            </form>
            <br>
        </td>

    </tr>

</table>
</body>
</html>

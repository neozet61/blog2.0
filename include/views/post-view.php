<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Пост: <?= $postTitle ?></title>
    <link rel="stylesheet" href="styles.css">
</head>
<body>
<table width="90%" border="0" align="center" bgcolor="white">
    <tr>
        <td colspan="5" align="left" width="80%">
            <h1><font color="gray">Пост: <?= $postTitle ?></font></h1>
            <hr>
        </td>
        <td colspan="1" align="left">
            <p>
                <a class="bot2" href="index.php">Главная</a>
            </p>
        </td>
    </tr>
    <tr>
        <td colspan="5" width="80%">
            <h2><?= $postTitle ?></h2>

                <p><b>Категория: </b><?= $categoryTitleById[$postCategoryId] ?> |
                    <b>Дата:</b> <?= date('d.m.Y H:i', $postTime) ?></p>

                <p><?= $postBody ?></p>

        </td>
        <td colspan="1" valign="top" align="left">
            <h2><font color="gray">Категории:</font></h2>
            <ul>
                <ul>
                    <?php include "./include/views/parts/nav-part-category.php"; ?>
                </ul>
            </ul>
        </td>
    </tr>
    <tr>
        <td colspan="5">
            <h2>Коментарии:</h2>
            <ul>
                <?php

                foreach ($comments as $comment) {
                    $commentId = $comment[FIELD_NUM_COMMENT_ID];
                    $commentUser = $comment[FIELD_NUM_COMMENT_USER_NICKNAME];
                    $commentBody = $comment[FIELD_NUM_COMMENT_BODY];
                    $commentTime = $comment[FIELD_NUM_COMMENT_TIME];
                    $commentPostId = $comment[FIELD_NUM_COMMENT_POST_ID];


                    if (isset($_COOKIE['lastvisit_' . $postId]) and
                            $_COOKIE['lastvisit_' . $postId] < $comment[FIELD_NUM_COMMENT_TIME]) {
                        $commentClass .= " new-comment";
                    }

                    echo "<div class='$commentClass'>";
                    echo '<div class="comment_header">';
                    echo $commentUser . " пишет (" . date('d.m.Y H:i', $commentTime) . "):";
                    echo '</div>';

                    echo '<div class="comment_body">';
                    echo $commentBody;
                    echo '</div>';

                    echo '</div>';

                }
                ?>

            </ul>
        </td>
    </tr>
    <tr>
        <td>
            <?php if ($isAuthed): ?>
                <h2>Добавить комментарий:</h2>

                <form action="index.php?page=comment-save" method="post">

                    <div class="form-login">
                        <label for="comment">Текст коментария:</label><br>
                        <textarea name="comment" id="comment" required placeholder="Ваш коментарий..."></textarea>
                        <input type="hidden" name="post_id" value="<?= $postId ?>">
                    </div>

                    <div class="form-login">
                        <button type="submit">Опубликовать</button>
                    </div>

                </form>
            <?php else: ?>
                <h2>Для добавления коментария необходима авторизация!</h2>
                <p>Авторизируйся по братски ;)</p>
                <a class="bot2" href="index.php?page=auth-form">Войти</a>
            <?php endif; ?>

        </td>
    </tr>

</table>
</body>
</html>

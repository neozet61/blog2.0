<?php

function dump(...$values)
{
    foreach ($values as $value) {
        echo "<pre>";
            print_r($value);
        echo "</pre>";
    }
}


function dd(...$values)
{
    dump(...$values);
    die();
}
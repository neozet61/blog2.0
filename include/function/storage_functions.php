<?php

function insert(string $space, int $id, array $data): void
{
    $filePath = _getFilePath($space, $id);

    $content = json_encode($data);

    file_put_contents($filePath, $content);     //создаем файл и пишем содержимое
}

function getById(string $space, int $id): ?array
{
    $filePath = _getFilePath($space, $id);

    if (!file_exists($filePath)) {
        return null;
    }

    return json_decode(file_get_contents($filePath));
}

function deleteById(string $space, int $id): void
{
    $filePath = _getFilePath($space, $id);

    if (!file_exists($filePath)) {
        return;
    }

    unlink($filePath);

}

function truncateSpace(string $space): void
{
    $spacePath = _getSpacePath($space);     //путь к папке

    $files = scandir($spacePath);
    array_shift($files);
    array_shift($files);
    array_shift($files);

    foreach ($files as $id) {

        $file = $spacePath . "/" . $id;
        unlink($file);
    }
}

function selectWhereFieldEqual(string $space, int $fieldNumber, mixed $value): array
{
    $spacePath = _getSpacePath($space);    //путь к папке

    $array = [];

    $files = scandir($spacePath);     //сканируем директорию
    array_shift($files);
    array_shift($files);

    foreach ($files as $fileName) {

        $filePath = $spacePath . "/" . $fileName;     //путь к файлу

        $fileContent = json_decode(file_get_contents($filePath));

        if ($fileContent[$fieldNumber] == $value) {
            $array[] = $fileContent;
        }
    }

    return $array;
}

function getAll(string $space): array
{
    $spacePath = _getSpacePath($space);    //путь к папке

    $array = [];

    $files = scandir($spacePath);     //сканируем директорию
    array_shift($files);
    array_shift($files);

    foreach ($files as $fileName) {

        $filePath = $spacePath . "/" . $fileName;     //путь к файлу

        $fileContent = json_decode(file_get_contents($filePath));

        $array[] = $fileContent;
    }
    return $array;
}

function getMaxIdForSpace(string $space): int
{
    $files = scandir(_getSpacePath($space));
    array_shift($files);
    array_shift($files);

    if (empty($files)) {
        return 0;
    }

    $max = 0;

    foreach ($files as $file) {
        $value = (int)explode('.', $file)[0];
        if ($value > $max) {
            $max = $value;
        }
    }

    return $max;
}

function _getSpacePath(string $space): string
{
    $spacePath = './include/storage/' . $space;

    if (!file_exists($spacePath)) {        //проверяем наличие папки
        mkdir($spacePath, 0777, true);        //создаем папку
    }

    return $spacePath;
}

function _getFilePath(string $space, int $id): string
{
    return _getSpacePath($space) . "/$id.json";     //путь к файлу
}

/*
function my_str_contains(string $haystack, string $needle): bool
{
    $counter = 0;
    $strLengthHaystack = strlen($haystack);
    $strLengthNeedle = strlen($needle);

    for ($i = 0, $i < $strLengthHaystack, $i++) {
        $currentCharHeystack = $haystack{$i};
        for ($k = 0, $k < $strLengthNeedle, $k++) {
            $currentCharNeedle = $needle{$k};
            if ($currentCharHeystack == $currentCharNeedle) {
                $counter++;
                if ()
            }
        }
    }

    return false;

    return true;
}
*/



